--
-- File generated with SQLiteStudio v3.4.4 on จ. ก.ย. 18 14:35:58 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: BRANCH
DROP TABLE IF EXISTS BRANCH;

CREATE TABLE BRANCH (
    BRC_CODE               CHAR (6)     NOT NULL,
    BRC_NAME               VARCHAR (35) NOT NULL,
    BRC_ADDRESS            VARCHAR (35) NOT NULL,
    BRC_AMOUNT_OF_EMPLOYEE SMALLING     NOT NULL,
    BRC_PHONE              CHAR (10)    NOT NULL,
    PRIMARY KEY (
        BRC_CODE
    )
);

INSERT INTO BRANCH (
                       BRC_CODE,
                       BRC_NAME,
                       BRC_ADDRESS,
                       BRC_AMOUNT_OF_EMPLOYEE,
                       BRC_PHONE
                   )
                   VALUES (
                       '000001',
                       'D-coffee',
                       'Bansaen',
                       25,
                       '0989089452'
                   );


-- Table: CHECK_INGREDIENTS
DROP TABLE IF EXISTS CHECK_INGREDIENTS;

CREATE TABLE CHECK_INGREDIENTS (
    CKIGD_CODE CHAR (6) NOT NULL,
    CKIGD_DATE DATE     NOT NULL,
    CKIGD_TIME TIME     NOT NULL,
    EMP_CODE   CHAR (6),
    PRIMARY KEY (
        CKIGD_CODE
    ),
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE
);


-- Table: CHECK_WORK
DROP TABLE IF EXISTS CHECK_WORK;

CREATE TABLE CHECK_WORK (
    CW_CODE          CHAR (6)      NOT NULL,
    CW_IN            DATETIME      NOT NULL,
    CW_OUT           DATETIME      NOT NULL,
    CW_WORKING_HOURS NUMBER (6, 2) NOT NULL,
    EMP_CODE         CHAR (6),
    PRIMARY KEY (
        CW_CODE
    ),
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE
);


-- Table: CUSTOMER
DROP TABLE IF EXISTS CUSTOMER;

CREATE TABLE CUSTOMER (
    CTM_CODE    CHAR (6)      NOT NULL,
    CTM_FNAME   VARCHAR (35)  NOT NULL,
    CTM_LNAME   VARCHAR (35)  NOT NULL,
    CTM_AGE     SMALLINT      NOT NULL,
    CTM_PHONE   CHAR (10)     NOT NULL,
    CTM_ADDRESS VARCHAR (35)  NOT NULL,
    CTM_EMAIL   VARCHAR (100) NOT NULL,
    CTM_POINT   SMALLINT      NOT NULL,
    PRIMARY KEY (
        CTM_CODE
    )
);

INSERT INTO CUSTOMER (
                         CTM_CODE,
                         CTM_FNAME,
                         CTM_LNAME,
                         CTM_AGE,
                         CTM_PHONE,
                         CTM_ADDRESS,
                         CTM_EMAIL,
                         CTM_POINT
                     )
                     VALUES (
                         '111111',
                         'somtum',
                         'pedmak',
                         35,
                         '0995999599',
                         'soy 8 chonburi',
                         'kaitom@gmail.com',
                         10
                     );


-- Table: EMPLOYEE
DROP TABLE IF EXISTS EMPLOYEE;

CREATE TABLE EMPLOYEE (
    EMP_CODE      CHAR (6)             NOT NULL,
    EMP_FNAME     VARCHAR (35)         NOT NULL,
    EMP_LNAME     VARCHAR (35)         NOT NULL,
    EMP_AGE       SMALLINT             NOT NULL,
    EMP_GENDER    CHAR (1)             NOT NULL,
    EMP_PHONE     CHAR (10)            NOT NULL,
    EMP_ADDRESS   VARCHAR (35)         NOT NULL,
    EMP_RANK      SMALLINT             NOT NULL,
    EMP_WAGES_PER [HOUR NUMBER] (8, 2) NOT NULL,
    EMP_EMAIL     VARCHAR (100)        NOT NULL,
    BRC_CODE      CHAR (6),
    PRIMARY KEY (
        EMP_CODE
    ),
    FOREIGN KEY (
        BRC_CODE
    )
    REFERENCES BRANCH
);

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '111111',
                         'somtum',
                         'pedmak',
                         35,
                         'male',
                         '0995999599',
                         'soy 8 chonburi',
                         1,
                         12345678.11,
                         'kaitom@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '112121',
                         'BOOM',
                         'TOD',
                         20,
                         'male',
                         '0990567802',
                         'soy 8 bangpree',
                         2,
                         123445.11,
                         'Boomeieii@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '331313',
                         'kai',
                         'Yang',
                         25,
                         'Female',
                         '0899359064',
                         'soy 12 pattaya',
                         3,
                         12452145.11,
                         'Yamyu12@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '411111',
                         'Sumyang',
                         'Aroi',
                         28,
                         'Female',
                         '0785999599',
                         'soy 18 chonburi',
                         1,
                         22545678.11,
                         'Sumyang@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '512121',
                         'YOMY',
                         'Numcha',
                         19,
                         'Female',
                         '0580567802',
                         'soy 1 bangpree',
                         1,
                         563445.11,
                         'YOMY@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '631313',
                         'PED',
                         'Yang',
                         18,
                         'male',
                         '0559452064',
                         'soy 2 pattaya',
                         3,
                         11112145.11,
                         'PED@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '456789',
                         'Yumee',
                         'Eiei',
                         24,
                         'Female',
                         '070254599',
                         'soy 9 chonburi',
                         1,
                         52142578.11,
                         'Yumee@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '789456',
                         'Yoona',
                         'Apodate',
                         18,
                         'Female',
                         '0805645214',
                         'soy 20 bangpree',
                         1,
                         666112.11,
                         'Yoona@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '852774',
                         'Jk',
                         'Toon',
                         21,
                         'male',
                         '0556551111',
                         'soy 18 pattaya',
                         3,
                         1122225.11,
                         'Toon@gmail.com',
                         '000001'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_WAGES_PER,
                         EMP_EMAIL,
                         BRC_CODE
                     )
                     VALUES (
                         '963456',
                         'Mewmew',
                         'TuTee',
                         20,
                         'Female',
                         '0954738644',
                         'soy 10 chonburi',
                         2,
                         600000.11,
                         'Meww@gmail.com',
                         '000001'
                     );


-- Table: INGREDIENTS
DROP TABLE IF EXISTS INGREDIENTS;

CREATE TABLE INGREDIENTS (
    ING_CODE  CHAR (6)        NOT NULL,
    ING_NAME  VARCHER (35)    NOT NULL,
    ING_PRICE NUMBER (6, 2)   NOT NULL,
    ING_TYPE  VARCHAR (10000) NOT NULL,
    ING_QOH   SMALLING        NOT NULL,
    ING_MIN   SMALLING        NOT NULL,
    PRIMARY KEY (
        ING_CODE
    )
);


-- Table: INGREDIENTS_DETAILS
DROP TABLE IF EXISTS INGREDIENTS_DETAILS;

CREATE TABLE INGREDIENTS_DETAILS (
    ING_D_CODE          CHAR (6)        NOT NULL,
    ING_D_CODE_DESCRIPT VARCHAR (10000) NOT NULL,
    ING_D_CODE_NAME     VARCHAR (35)    NOT NULL,
    ING_D_CODE_QOH      SMALLING        NOT NULL,
    ING_CODE            CHAR (6),
    CKIGD_CODE          CHAR (6),
    PRIMARY KEY (
        ING_D_CODE
    ),
    FOREIGN KEY (
        ING_CODE
    )
    REFERENCES INGREDIENTS,
    FOREIGN KEY (
        CKIGD_CODE
    )
    REFERENCES CHECK_INGREDIENTS
);


-- Table: ODER_DETAIS
DROP TABLE IF EXISTS ODER_DETAIS;

CREATE TABLE ODER_DETAIS (
    ORD_D_CODE        CHAR (6)      NOT NULL,
    ORD_D_NAME        VARCHAR (35)  NOT NULL,
    ORD_D_AMOUNT      SMALLING      NOT NULL,
    ORD_D_UNIT_PTICE  NUMBER (6, 2) NOT NULL,
    ORD_D_TOTAL_PTICE NUMBER (6, 2) NOT NULL,
    ORD_CODE          CHAR (6),
    ING_CODE          CHAR (6),
    PRIMARY KEY (
        ORD_D_CODE
    ),
    FOREIGN KEY (
        ORD_CODE
    )
    REFERENCES ORDERING,
    FOREIGN KEY (
        ING_CODE
    )
    REFERENCES INGREDIENTS
);


-- Table: ORDERING
DROP TABLE IF EXISTS ORDERING;

CREATE TABLE ORDERING (
    ORD_CODE   CHAR (6)        NOT NULL,
    ORD_DATE   DATE            NOT NULL,
    ORD_AMOUNT SMALLING        NOT NULL,
    ORD_PRICE  NUMBER (6, 2)   NOT NULL,
    ORD_TYPE   VARCHAR (10000),
    EMP_CODE   CHAR (6),
    PRIMARY KEY (
        ORD_CODE
    ),
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE
);


-- Table: PAY_SALARY
DROP TABLE IF EXISTS PAY_SALARY;

CREATE TABLE PAY_SALARY (
    PS_CODE                CHAR (6)      NOT NULL,
    PS_SALARY_PATMENT_DATE DATE          NOT NULL,
    PS_WORKING_HOURS       SMALLING      NOT NULL,
    PS_WAGE_RATE           NUMBER (6, 2) NOT NULL,
    PS_STATUS              CHAR (1)      NOT NULL,
    EMP_CODE               CHAR (6),
    CW_CODE                CHAR (6),
    PRIMARY KEY (
        PS_CODE
    ),
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE,
    FOREIGN KEY (
        CW_CODE
    )
    REFERENCES CHECK_WORK
);


-- Table: PRODUCT
DROP TABLE IF EXISTS PRODUCT;

CREATE TABLE PRODUCT (
    PD_CODE     CHAR (6)       NOT NULL,
    PD_NAME     VARCHAR (35)   NOT NULL,
    PD_PRICE    NUMBER (6, 2)  NOT NULL,
    PD_DESCRIPT VACHAR (10000) NOT NULL,
    PRIMARY KEY (
        PD_CODE
    )
);

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000000',
                        'aaaaa',
                        100000,
                        'roitorortotr'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000001',
                        'coffee',
                        50,
                        'hehehehehe'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000002',
                        'cake chocolate',
                        150,
                        'sweet'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000003',
                        'butter cake',
                        60,
                        'sweet'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000004',
                        'strawberry',
                        45,
                        'sweet'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000005',
                        'Espresso',
                        60,
                        'coffee'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000006',
                        'Amaricano',
                        55,
                        'coffee'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000007',
                        'cookie',
                        25,
                        'dessert'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000008',
                        'bubery cake',
                        35,
                        'dessert'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000009',
                        'yam role',
                        20,
                        'dessert'
                    );

INSERT INTO PRODUCT (
                        PD_CODE,
                        PD_NAME,
                        PD_PRICE,
                        PD_DESCRIPT
                    )
                    VALUES (
                        '000010',
                        'Latte',
                        55,
                        'coffee'
                    );


-- Table: PROMOTION
DROP TABLE IF EXISTS PROMOTION;

CREATE TABLE PROMOTION (
    PMT_CODE CHAR (6)     NOT NULL,
    PMT_NAME VARCHAR (35) NOT NULL,
    PMT_DATE DATE         NOT NULL,
    PMT_EXD  DATE         NOT NULL,
    PRIMARY KEY (
        PMT_CODE
    )
);


-- Table: PROMOTION_DETILS
DROP TABLE IF EXISTS PROMOTION_DETILS;

CREATE TABLE PROMOTION_DETILS (
    PMT_D_CODE       CHAR (6)      NOT NULL,
    PMT_D_COUNDITION VARCHAR (35)  NOT NULL,
    PMT_D_DISCOUNT   NUMBER (6, 2) NOT NULL,
    PD_CODE          CHAR (6),
    PMT_CODE         CHAR (6),
    PRIMARY KEY (
        PMT_D_CODE
    ),
    FOREIGN KEY (
        PD_CODE
    )
    REFERENCES PRODUCT,
    FOREIGN KEY (
        PMT_CODE
    )
    REFERENCES PROMOTION
);


-- Table: RECEIPT
DROP TABLE IF EXISTS RECEIPT;

CREATE TABLE RECEIPT (
    RECE_CODE     CHAR (6)      NOT NULL,
    RECE_DATE     DATE          NOT NULL,
    RECE_TOTAL    NUMBER (6, 2) NOT NULL,
    RECE_DISCOUNT NUMBER (6, 2) NOT NULL,
    RECE_CHANGE   NUMBER (6, 2) NOT NULL,
    RECE_PAYMENT  NUMBER (6, 2) NOT NULL,
    EMP_CODE      CHAR (6),
    CTM_CODE      CHAR (6),
    BRC_CODE      CHAR (6),
    PRIMARY KEY (
        RECE_CODE
    ),
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE,
    FOREIGN KEY (
        CTM_CODE
    )
    REFERENCES CUSTOMER,
    FOREIGN KEY (
        BRC_CODE
    )
    REFERENCES BRANCH
);


-- Table: RECEIPT_DETILS
DROP TABLE IF EXISTS RECEIPT_DETILS;

CREATE TABLE RECEIPT_DETILS (
    RC_D_CODE         CHAR (6)      NOT NULL,
    RC_D_PD_NAME      VARCHAR (35)  NOT NULL,
    RC_D__PD_AMOUNT   SMALLINT      NOT NULL,
    RC_D__UNIT_PRICE  NUMBER (6, 2) NOT NULL,
    RC_D__TOTAL_PRICE NUMBER (6, 2) NOT NULL,
    RC_D_DISCOUNT     NUMBER (6, 2) NOT NULL,
    PRIMARY KEY (
        RC_D_CODE
    )
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
